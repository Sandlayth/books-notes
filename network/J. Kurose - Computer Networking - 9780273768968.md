# J. Kurose - Computer Networking (9780273768968)

## Chapter 1: Computer Networks & the Internet

### 1.1 What is Internet ?

Basic approach ⇒ interconnect of devices (hosts and end systems)

    - communication links and packets switchs

    - through ISPs, set of networks of communication links & packet switchs

    - protocols: TCP/IP

    - Internet standards: IETF ⇒ RFCs

Service approach ⇒ distributed applications

    ↳ API specifies how to use part of the application

### 1.2 The Network Edge

Physical media:

    - guided media (fiber, cable, ...)

    - unguided media (wifi, satellite, ...)

- Twisted-Pair copper wire

        - cheap

        - twisted to reduce electrical interferences from close by pairs

        - Unshielded Twisted Pairs (UTP) used for LAN

        - from 10Mbps to 10Gbps

- Coaxial cable

        - 2 copper conductors concentric

        - guided shared medium

            → shift signal to specific Hz

            → TV & Internet

        - ~10Mbps

* Fiber Optics

        - 100s Km

        - 51,8 Mbps → 39,8 Gbps

* Terretrial radio

        - short distance

            - 10 → 100m

            - LAN

        - wide area

            - 100s Km

            - Cellular

* Satellite Radio

        - Geostationary satellite

            - same spot on earth

            → 36 000 Km in orbit

            → delays signal 280ms

        - Low Earth Orbiting (LEO) satellite

            - rotate like the moon

            ↳ many satellites required for max coverage

### 1.3 Internet Core

#### 1.3.1 Packet switching

Packet switches store and forward

    - wait for the packet to arrive entirely to forward it

        → stores it in the buffer ⇒ delay

d(end_to_end) = N × L / R

| d(end_to_end) = | N ×                        | L/             | R                 |
| --------------- |:-------------------------- |:-------------- | ----------------- |
| delay           | Number of links in between | Size of packet | Transmission rate |

Packet is stored in output buffer

    → queuing delay

    → if buffer full

        → packet loss / congestion

Packet is forwarded to the route according to the forwarding table

#### 1.3.2 Circuit switching

resources exclusively reserved

constant rate guaranteed

circuit in link 2 ways:

    - Frequency division multiplexing (FDM)

        → width of band = bandwidth

        → phones have width of 4KHz

        → FM radio shares between 88MHz and 108MHz

    - Time division multiplexing (TDM)

        → each connection has time slot

circuit switching idle during silent periods

#### 1.3.3 Network of networks

Connect all ISPs together by mesh is too hard

Access ISP → intermediary ISPs → Tier-1 ISP

Tier-1 don't pay anyone

    ↳ ∅ customer but only provider

Internet = ISP links + Points of Presence (PoP) + multi-homing + peering + Internet Exchange Points (IXP)

PoP = 1 or more router in the same location of provider with high speed link leased

customer ISP usually multi home

    → connect to 2 or more provider ISPs

ISPs of same level peer to reduce costs

3rd party company creates IXP: meeting point to peer ISPs

Content providers (ex Google)

    → private network carries only reserved traffic

    → bypass upper-tiers by peering with Tier-1 ISP, IXP and access ISPs

### 1.4 Delay, Loss and Throughput

Delay = 

    - Processing delay

        → examines header and determines where to send

        → checks bit level errors

        → ~ µs

    - Queuing delay

        → ~ µs ⇒ ms

    - Transmission delay

         → time for router to push packet

         → ~ µs ⇒ ms

    - Propagation delay

d_nodal = d_proc + d_queue + d_trans + d_prop

Queuing delay

    if traffic intensity > 1 ⇒ redesign system

traffic intensity = (L × a) / R

| (L ×              | a )/         | R                 |
| ----------------- | ------------ | ----------------- |
| number of packets | arrival rate | transmission rate |

Packet loss: if queue full, router drop packet

Traceroute

    - sends 3 packets to each hop

    - if < 3 packets back, packet is lost and displays *

Throughput

    → rate at which a host receives data (b/s)

    → bottleneck link can be caused if one link on network has less rate

### 1.5 Protocol layers and their service models

#### 1.5.1 Layered architecture

a) Five-layers Internet Protocol stack

* Application

* Transport

* Network

* Link

* Physical

b) Seven-layers OSI stack

* Application

* Presentation

* Session

* Transport

* Network

* Link

* Physical

Application Layer

    - packet = message

Transport Layer

    - TCP & UDP

    - controls flow

    -packet = segment

Network Layer

    - IP Protocol

    - routing informations

    - packet = datagram

Link Layer

    - packet = frame

Physical Layer

    - bits

#### 1.5.2 Encapsulation

Each layer encapsulate layer below:

    - header field

    - payload field

## Chapter 2: Application Layer

### 2.1 Principles of Network Applications

#### 2.1.1 Network Application Architectures

client-server architecture:

    - always-on host: server

        - fixed know IP

        - ex: Web, email, FTP, ...

P2P architecture

    - direct communication with peers

    - ex: file-sharing, Internet Phone, IPTV, ...

    - self scalability

    - challenges:

        - ISP friendly = asymetrical bandwidth

        - security

        - incentive

#### 2.1.2 Processes Communicating

processes communicate by exchanging messages

##### Client and Server Processes

process initiating communication is the client

##### The Interface between the process and the computer networks

process sends messages an receive them through socket

    → interface between application and transport layers

    → API between application and network

        - choose transport protocol

        - choose transport layer parameters: maximum bytes/segment size...

##### Addressing Processes

Address of Host: IP Address

Process identifier: port number

#### 2.1.3 Transport Services Available to Applications

##### Reliable Data Transfer

guarantees data delivery service

if ∅ reliable = loss-tolerant application (ex: audio/video)

##### Throughput

app with bandwidth requirements = bandwidth sensitive applications

app with no bandwidth requirement = elastic bandwidth application

##### Timing

real-ime applications need short delays

##### Security

encrypt/decrypt data

#### 2.1.4 Transport Services Provided by the Internet

| Application                             | Data Loss         | Throughput            | Time-Sensitive    |
| --------------------------------------- | ----------------- | --------------------- | ----------------- |
| File transfer/download                  | No loss           | Elastic               | No                |
| E-mail                                  | No loss           | Elastic               | No                |
| Web documents                           | No loss           | Elastic (few kbps)    | No                |
| Internet telephony / Video conferencing | Loss-tolerant     | Audio: few kbps–1Mbps |                   |
| Video: 10 kbps–5 Mbps                   | Yes: 100s of msec |                       |                   |
| Streaming stored audio/video            | Loss-tolerant     | Same as above         | Yes: few seconds  |
| Interactive games                       | Loss-tolerant     | Few kbps–10 kbps      | Yes: 100s of msec |
| Instant messaging                       | No loss           | Elastic               | Yes and No        |

##### TCP services

connection-oriented service

reliable data transfer service

##### UDP services

connection-less

no congestion control

##### Securing TCP

no security in TCP

→ extra layer of SSL (Secure Socket Layer)

    ↳ own API with libraries that need to be used server and client sides

##### Services not provided by Internet transport protocols

throughput and timing not guaranteed with TCP & UDP

#### 2.1.5 Application-Layer Protocols

Defines:

* types of message (ex: request or answer)

* syntax of messages

* semantic of fields

* rules to determine when and how process send messages

### 2.2 The Web and HTTP

#### 2.2.1 Overview of HTTP

HyperText Transfer Protocol

Terminology:

    - Web page (document) = objects

    - URL = hostname & path

over TCP

stateless protocol

#### 2.2.2 Non-persistent and persistent connections

same socket used for multiple requests and same client ?

✓ → persistent

✗ → non persistent

##### HTTP with non-persistent connection

1. Client initiate TCP connection to server on port 80 → a socket per client and server

2. Client sends HTTP request via its socket

3. Server processes request an send response via its socket

4. Server tells TCP to close connection

5. Client receives response

All steps from 1 to 5 are repeated for every request

RTT = round-trip time = time for a request to et answer

For every request:

* TCP buffer allocated

* TCP variables set

* RTT of TCP connection reiterated

##### HTTP with Persistent connection

server does not ask immediately to close connection

requests can be made back-to-back = pipelining

server closes after timeout

#### 2.2.3 HTTP Message Format

##### HTTP request Message

```
GET /somedir/page.html HTTP/1.1
Host: www.someschool.edu
Connection: close
User-agent: Mozilla/5.0
Accept-language: fr
```

first line = request line

    → method: {GET, POST, PUT, HEAD, DELETE}

    → object path

    → HTTP version

other lines = header lines

    → host (used for web proxy caches)

    → connection (persistance)

    → user-agent to send appropriate browser content

![figure 2.8.png](.\9780273768968-resources\figure%202.8.png)

##### HTTP response message

```
HTTP/1.1 200 OK
Connection: close
Date: Tue, 09 Aug 2011 15:44:04 GMT
Server: Apache/2.2.3 (CentOS)
Last-Modified: Tue, 09 Aug 2011 15:11:03 GMT
Content-Length: 6821
Content-Type: text/html
(data data data data data ...)
```

* first line = status line

* six header lines

* entity body

![figure 2.9.png](.\figure%202.9.png)

#### 2.2.4 User-Server interaction: Cookies

![figure 2.10.png](.\9780273768968-resources\figure%202.10.png)

#### 2.2.5 Web Caching

Web caching is proxy server

1. TCP connection to web cache

2. web cache checks if copy exists to send it

3. if not, make requests on behalf

4. stores and sends answer

Usually used by universities and ISPs

CDNs (Content Distribution Networks)

    ↳ distributed cache servers throughout the internet

#### 2.2.6 Conditional GET

Proxy can check if version has been updated with header

`If-Modified-Since: ....`

If yes, gets the new version, if no gets:

```
HTTP/1.1 304 Not Modified
Date: Sat, 15 Oct 2011 15:39:29
Server: Apache/1.3.0 (Unix)
(empty entity body)
```

### 2.3 File Transfer: FTP

2 parallels connections

    - control connection (port 21)

        - send control informations (user id, password, commands, ...)

    - data connection (port 20)

       - send files

    ⇒ control informations are sent out of band

stateful connection

### 2.4 Electronic Mail in the Internet

Simple Mail Transfer Protocol (SMTP)

#### 2.4.1 SMTP

port 25/TCP

example transcript of SMTP

```telnet
S: 220 hamburger.edu
C: HELO crepes.fr
S: 250 Hello crepes.fr, pleased to meet you
C: MAIL FROM: <alice@crepes.fr>
S: 250 alice@crepes.fr ... Sender ok
C: RCPT TO: <bob@hamburger.edu>
S: 250 bob@hamburger.edu ... Recipient ok
C: DATA
S: 354 Enter mail, end with “.” on a line by itself
C: Do you like ketchup?
C: How about pickles?
C: .
S: 250 Message accepted for delivery
C: QUIT
S: 221 hamburger.edu closing connection
```

### 2.4.2 Comparison with HTTP

* push protocol

* requires to be encoded in 7-bits ASCII

* All objects are sent in a single message

#### 2.4.3 Mail Message Formats

```header
From: alice@crepes.fr
To: bob@hamburger.edu
Subject: Searching
```

#### 2.4.4 Mail Access Protocols

##### POP3

port 110/TCP

3 phases:

    - authorization (user/password)

    - transaction (read `retr`, list `list`, delete `dele`)

    - update (apply deletions)

##### IMAP

Assign email to folders

    → default INBOX

Can download part of multipart MIME Message

### 2.5 DNS - The Internet Directory Service

#### 2.5.1 Service provided by DNS

* Translating hostnames to IP

* Host aliasing

* Mail server aliasing

* Load distribution

#### 2.5.2 Overview of how DNS works

##### A distributed, hierarchical database

* Root DNS server (13 root servers worldwide, lettered from A to M)

* Top-Level Domain (TLD) server

* Authoritative DNS server

![figure 2.21.png](.\9780273768968-resources\figure%202.21.png)

#### 2.5.3 DNS Records and Messages

DNS servers store Resources Records (RR)

RR → (Name, Value, Type, TTL)

##### DNS Messages

![figure 2.23.png](.\9780273768968-resources\figure%202.23.png)

### 2.6 Peer-to-Peer Applications

#### 2.6.1 P2P File Distribution

Case of BItTorrent:

→ torrent is collection of peers sharing a file

→ to join torrent inform the responsible node: the tracker

→ when joinded, tracker selects random IPs and send them to you

→ ask from each peers chunks of torrent

    → rarest first: request the rarest chunks to redistributed the load and prevent loss

#### 2.6.2 Distributed Hash Tables (DHT)

each peer holds subset of adata

    ↳ request peer to get any data

→ each peer gets an id on the range `[0; 2^n -1]` for a given n

→ associate numbered hash of object to id

→ if peer absent, get closed one
