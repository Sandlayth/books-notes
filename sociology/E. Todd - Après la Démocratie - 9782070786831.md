# E. Todd - Après la démocratie (9782070786831)

## Introduction:

Sarkozy (et Royal) → vide politique

    ↳ ni gauche ni droite

Société folle élit fou

    → incohérence pensée

         → vide idéologique/religieux

    → médiocrité intellectuelle

          → stagnation éducative

    → aggressivité

        → démocratie ⇒ exclusion du non citoyen

    → amour de l'argent

    → instabilité affective & familiale

        → évolution des valeurs familiales

## Chapitre 1: Le vote est religieux

 mélange des idéologiques politiques

    → votes FN pour l'UMP

    → PCF décroissant

    → FN réunit anciens communistes et commerçants RPR

Timothy Tackett

    ↳ carte des prêtes qui acceptent/refusent constitution civile du Clergé votée le 12/07/1790

        ↳ élection des prêtres par les fidèles ⇒ ∅ pape

    ⇒  France déchristianisée vs France catholique (droite)

* ∅ religieux : bassin parisien et méditerranéen

* ✓ religieux: constellation ici et là

entre 1880 et 1980 → la France se déchristianise

    ↳ extrémisme gaulliste communiste

1978, PCF 20% ⇒ France déchristianisée

→ à partir de Vatican II

    ↳ déclin christianisme

Pourcentages de catholiques allant à la messe

| 1948                       | 1968 | 1988 | 2007                    |
| -------------------------- | ---- | ---- | ----------------------- |
| 37%                        | 25%  | 13%  | 8%                      |
| (>50% de La france rurale) |      |      | (3% de personnes agées) |

### Décomposition politique

Catholique était contre balance

↳ électeurs catho → PS entre 67 et 78

PCF s'effondre avec l'église

FN émerge élections EU de 1984

    ↳ départements laïques

90's ∅ croyances solides, droite et gauche se mélangent

⇒ 2002 chirac 88,2% en l'absence d'alternative

### Trahisons comparées

PS perd les valeurs de gauche

    ↳ Lamy directeur de OMC

    ↳ DSK directeur général du FMI

Droite perd ses valeurs chrétiennes

    → soumission sociale

    → responsabilité

    → charité

    → désintéressement

RPR renonce à la nation & PS aux ouvriers

    → Chirac → Europe

    → Sarkozy → USA

Philippe Séguin

    - 1992 s'oppose au traité de Maastricht

    - 2005 se tait face à la constitution européenne

    ⇒ modèle de trahison politique pour gravir les échelons

### Primauté de la décomposition religieuse

gaullisme ⇒ culture du chef

    ↳ effondrement sous VGE terminé par Chirac

effondrement du catholiscisme dans les 60's

entre 1965 et 2007, communisme et nazisme offrent alternative au salut

### Athéisme difficile

triomphe athéisme → liberté

    → ∅ Dieu → manque/anxiété

        ↳ vit pour critiquer le christianisme

    → plus de chrétiens ⇒ athée doute sur le sens de la vie/mort

        ↳ recherche sensations (argent, sexualité, violences)

    → sécurité sociale et confort matériel comblent la peur métaphysique

        ↳ US ∅ sécurité sociale ⇒ retour au métaphysique

### Incroyance à islamophobie

  succède à l'arabophobie des 80's

    → incompréhension des moeurs

    → chômage

UMP voit islam pour rallier FN

Théorie du choc des civilisations (Samuel Huntington 1993)

    → guerre de l'occident à l'islam et la chine

    → choc impérialiste

        ↳ contrôle USA sur Arabie

        ↳ invasion de l'Irak

    → narcissime culture occidental date avant Al Qaida

    → laïcité vit à travers critique religieuse

        ↳ disparition du catholiscisme → islam

## Chapitre 2: Stagnation éducative & pessimisme culturel

Sarkozy & gouvernement ∅ études élites

→ libération conformisme FR (sciences Po + ENA)

→ crise éducative ⇒ ∅ diplôme est un avantage électoral

conformisme PS > UMP

    ↳ UMP contre balance avec religion/argent/De Gaulle

ségolènisme  ⇒ émancipation du dogme scolaire

    ↳ 95e/100 à l'ENA

    ↳ ∅ ENA dans les conseillers

politique FR = pensée fixée par formation VS pensée rebelles

conformisme néolibéral

        → libre-échange

        → ajustement structurel

        → flexibilité

        → moins de revenus et plus d'inégalités

    ↳ opposition ⇒ ∅ programme

        → démocratie participative

        → affirmation identitaire

            ↳ drapeau

            ↳ immigrés

### Stagnation éducative et pessimisme culturel

niveau éducatif stagne depuis 1990-1995

sortants sans qualification

| 1965 | 1980 | 1990 | jusqu'à aujourd'hui |
| ---- | ---- | ---- | ------------------- |
| 35%  | 15%  | 7,5% | 7,5%                |

obtention du bac

| 1950 | 1960  | 1970  | 1980  | 1990  | 2007  |
| ---- | ----- | ----- | ----- | ----- | ----- |
| 4,8% | 11,3% | 16,7% | 25,9% | 27,9% | 34,3% |

⇒ hausse globale du niveau moyen

    ↳ illusion d'une société scolairement inerte

∅ progrès

    ↳ ∅ progression du nb de bacheliers

    ↳ pessimisme culturel ⇒ sentiment de déclin

        ↳ "Défaite de la pensée" Finkielkraut 1987

        ↳ critique des moeurs de Muray entre 1985 et 2006

pessimisme culturel

    → "La France qui tombe" Nicolas Baverez

        → économie

        → diplomatie

        → intégration

        → laïcité

        → armée

        → recherche

        ↳ pessimisme renvoie aux 30's

        ⇒ rassemble droite et gauche

### Vide contre vide: national-républicanisme contre pensée unique

∅ idéologie ⇒ idéologie doit être tournée vers futur et action

⇒ pensée unique panglossienne

    → libre-échange sur terre entière

⇒ national-républicanisme 1988-2007

    ↳ pensée de contre-poids vide et tourné sur passé fantasmé

→ éducation patine dans socétés développées

    ↳ mutation technologique réduit désespoir

→ globalisation = mécanisme économique et financier

→ mondialisation = ouverture mentale des cultures

sarkozysme = superpose pensée vide et national-républicanisme

    ↳ retour idée nation

    ↳ retour à l'ordre

    ↳ flexibilisation marché emploi

    ↳ baisse coût travail → revenus

### Le sens de l'histoire : le niveau éducatif dans la longue durée: 1690 - 2008

dév éducation ⇒ variable primordiale

    → décollage alphabétisation → industrie

dév éducation ⇒ dév économoqie

    → "matérialisme historique

Taux alphabétisation jeune

|     | 1690 | 1790 | 1875 | 1881 | 1911 |
| --- | ---- | ---- | ---- | ---- | ---- |
| ♂   | 29%  | 47%  | 78%  | 86%  | 96%  |
| ♀   | 14%  | 27%  | 66%  | 79%  | 96%  |

histoire gouvernementale

    - 1833: loi Guizot > 1 école primaire obligatoire par commune

    - 1882: instruction obligatoire

    - 1959: Loi Berthoin > école 16 ans

    - 1975: réforme Hoby > collège unique

→ croissance alphabétisation entre fin 17e et 18e siècles

    ⇒ éducation ∅ légiférée

    ⇒ invention imprimerie et réforme protestante

lycée reste privilège bourgeois jusqu'à fin 2e GM

taux scolarisation stagne entre les 2 guerres

    ↳ niveau moyen augmente par disparition des âgés

    ⇒ pessimisme culturel

2e GM relance éducation

nombre étudiants

| 1950 | 1960 | 1970 | 1980   | 1990   | 1995   | 2006   |
| ---- | ---- | ---- | ------ | ------ | ------ | ------ |
| 200K | 310K | 850K | 1,175M | 1,717M | 2,179M | 2,275M |

⇒ stagnation à niveau élevé

### Le modèle américain

stagnation éducation ⇒ France suit USA avec 30 ans de retard

1880, alphabétisation USA 92%

60's, palier/plafond atteint

    → test aptitudes baissent (SAT scores)

pessimisme culturel USA

    → 1967-1980 ⇒ backslash

        - anti intellectualisme de la droite républicaine

        - sensibilité religieuse

### Optimisme britannique

Taux alphabétisation jeune

|     | 1790 | 1875 |
| --- | ---- | ---- |
| ♂   | 62%  | 82%  |
| ♀   | 40%  | 76%  |

révolution industrielle de 1750

    - déqualification emploi

    - déracinement

    - explosion démographique

    - paupérisme

    ⇒ stagnation éducative entre 1750 et 1882

| 1970 | 1980 | 1990   | 2004   |
| ---- | ---- | ------ | ------ |
| 621K | 827K | 1,079M | 2,494M |

années Blair (1997 - 2007)

    - optimisme social et culturel

    - baisse chômage <= augmentation des inégalités

    - bond niveau éducatif entre 90's et 2005

### Peut-on parler de causes ?

facteurs hypothétiques stagnation

    - TV

        ↳ retour culture orale

        ↳ rapport passif au divertissement & culture

        ⇒ arrivée internet ⇒ retour culture écrite

recherche cause ∅ priorité

    → dans l'Histoire, accélérations et ralentissements

    → stagnation actuelle = pause ou arrêt ?
